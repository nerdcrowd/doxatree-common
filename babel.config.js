module.exports = {
  plugins: [
    [
      "module-resolver",
      {
        alias: {
          "~": "./lib",
        },
      },
    ],
  ],
  presets: [
    [
      "@babel/preset-env",
      {
        targets: {
          node: "current",
        },
      },
    ],
  ],
};
