const Sequelize = require("sequelize");
const Models = require("./models");

class Database {
  connect(uri) {
    if (this.sequelize)
      return {
        connection: this.sequelize,
        models: this.models,
      };

    return new Promise(async (resolve, reject) => {
      try {
        this.sequelize = new Sequelize(uri);
        await Models.sync(this.sequelize);
        this.models = this.sequelize.models;

        setTimeout(resolve, 1000, {
          statusCode: 200,
          message: "Connected successfully!",
          connection: this.sequelize,
          models: this.models,
        });
      } catch (e) {
        reject(e);
      }
    });
  }
}

module.exports = Database;
