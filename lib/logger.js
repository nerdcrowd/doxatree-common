"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const chalk = require("chalk");
const loglevel = require("loglevel");
const loglevelPluginPrefix = require("loglevel-plugin-prefix");
// Set the default logging level
loglevel.setLevel('debug');
loglevelPluginPrefix.reg(loglevel);
const colors = {
  TRACE: chalk.magenta,
  DEBUG: chalk.cyan,
  INFO: chalk.blue,
  WARN: chalk.yellow,
  ERROR: chalk.red,
};
exports.setLoggingLevel = (level) => {
  loglevel.setLevel(level);
};
const createLogger = (name) => {
  const logger = loglevel.getLogger(name);
  loglevelPluginPrefix.apply(logger, {
    levelFormatter: level => level.toUpperCase(),
    nameFormatter: name => name || '(unknown logger)',
    timestampFormatter: timestamp => `[${timestamp.toISOString()}]`,
    format: (levelWrongType, nameWrongType, timestampWrongType) => {
      // Fixing the type definitions of the format function
      // logLevelPluginPrefix specifies argument types as if they came directly from logLevel,
      // but, in fact, they get passed through formatters before that (and therefore become strings)
      const level = levelWrongType;
      const name = nameWrongType;
      const timestamp = timestampWrongType;
      const template = (level, name, timestamp) => `${timestamp} ${level} ${name}:`;
      const applyLevelColor = colors[level];
      if (level === 'WARN' || level === 'ERROR') {
        return applyLevelColor(template(level, name, timestamp));
      }
      return template(applyLevelColor(level), chalk.green(name), chalk.gray(timestamp));
    },
  });
  return logger;
};
exports.default = createLogger;