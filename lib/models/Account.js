const { DataTypes, Model } = require("sequelize");

class Account extends Model {
  static init(sequelize) {
    super.init(
      {
        name: DataTypes.STRING,
        disabled: DataTypes.BOOLEAN,
      },
      {
        sequelize,
      }
    );
    return this;
  }

  static associate(models) {
    this.hasMany(models.Registry);
    this.hasMany(models.Metadata);
  }
}

module.exports = Account;
