const { DataTypes, Model } = require("sequelize");

class Asset extends Model {
  static init(sequelize) {
    super.init(
      {
        name: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        native: {
          type: DataTypes.BOOLEAN,
          defaultValue: false,
        },
        qty: {
          type: DataTypes.DOUBLE,
          allowNull: false,
          defaultValue: 0,
          min: 0,
        },
        metaPercentage: {
          type: DataTypes.DOUBLE,
          allowNull: false,
          defaultValue: 0,
          min: 0,
          max: 1,
        },
        currency: {
          type: DataTypes.ENUM,
          values: ["BRL", "USD"],
        },
      },
      {
        sequelize,
      }
    );
    return this;
  }

  static associate(models) {
    this.belongsTo(models.Ticker);
    this.belongsTo(models.Locket);
  }
}

module.exports = Asset;
