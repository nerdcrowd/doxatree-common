const { DataTypes, Model } = require("sequelize");

class Balance extends Model {
  static init(sequelize) {
    super.init(
      {
        name: {
          type: DataTypes.STRING,
          defaultValue: "Saldo Livre",
        },
        native: {
          type: DataTypes.BOOLEAN,
          defaultValue: false,
        },
        metaPercentage: {
          type: DataTypes.DOUBLE,
          allowNull: false,
          defaultValue: 1,
          min: 0,
          max: 1,
        },
      },
      {
        sequelize,
      }
    );
    return this;
  }

  static associate(models) {
    this.hasOne(models.Locket);
  }
}

module.exports = Balance;
