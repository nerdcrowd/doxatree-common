const { DataTypes, Model } = require("sequelize");

class Group extends Model {
  static init(sequelize) {
    super.init(
      {
        name: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        metaPercentage: {
          type: DataTypes.DOUBLE,
          allowNull: false,
        },
      },
      {
        sequelize,
      }
    );
    return this;
  }

  static associate(models) {
    this.belongsTo(models.User);
    this.hasMany(models.Locket);
  }
}

module.exports = Group;
