const { DataTypes, Model } = require("sequelize");

class Locket extends Model {
  static init(sequelize) {
    super.init(
      {
        name: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        metaPercentage: {
          type: DataTypes.DOUBLE,
          allowNull: false,
          defaultValue: 0,
          min: 0,
          max: 1,
        },
        targetPercentage: {
          type: DataTypes.DOUBLE,
          allowNull: false,
          defaultValue: 0,
          min: 0,
          max: 1,
        },
        floorPercentageAlert: {
          type: DataTypes.DOUBLE,
          allowNull: false,
          defaultValue: 0,
          min: 0,
          max: 1,
        },
        ceilPercentageAlert: {
          type: DataTypes.DOUBLE,
          allowNull: false,
          defaultValue: 0,
          min: 0,
          max: 1,
        },
        color: {
          type: DataTypes.STRING,
          defaultValue: "#e3a01a",
        },
        isDefault: {
          type: DataTypes.BOOLEAN,
          defaultValue: false,
        },
      },
      {
        sequelize,
      }
    );
    return this;
  }

  static associate(models) {
    this.belongsTo(models.Portfolio);
    this.belongsTo(models.Balance);
    this.hasMany(models.Asset);
  }
}

module.exports = Locket;
