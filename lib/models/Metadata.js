const { DataTypes, Model } = require("sequelize");

class Metadata extends Model {
  static init(sequelize) {
    super.init(
      {
        balanceUSD: {
          type: DataTypes.DOUBLE,
          allowNull: false,
          defaultValue: 0,
          min: 0,
        },
        balanceBRL: {
          type: DataTypes.DOUBLE,
          allowNull: false,
          defaultValue: 0,
          min: 0,
        },
      },
      {
        sequelize,
      }
    );
    return this;
  }

  static associate(models) {
    this.belongsTo(models.Account);
    this.belongsTo(models.User);
  }
}

module.exports = Metadata;
