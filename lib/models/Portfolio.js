const { DataTypes, Model } = require("sequelize");

class Portfolio extends Model {
  static init(sequelize) {
    super.init(
      {
        freeBRL: {
          type: DataTypes.DOUBLE,
          defaultValue: 0,
        },
        freeUSD: {
          type: DataTypes.DOUBLE,
          defaultValue: 0,
        },
        disabled: {
          type: DataTypes.BOOLEAN,
          defaultValue: false,
        },
      },
      {
        sequelize,
      }
    );
    return this;
  }

  static associate(models) {
    this.belongsTo(models.User);
    this.hasMany(models.Locket);
  }
}

module.exports = Portfolio;
