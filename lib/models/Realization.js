const { DataTypes, Model } = require("sequelize");

class Realization extends Model {
  static init(sequelize) {
    super.init(
      {
        stockPrice: {
          type: DataTypes.DOUBLE,
          allowNull: false,
        },
        qty: {
          type: DataTypes.DOUBLE,
          allowNull: false,
        },
        costs: {
          type: DataTypes.DOUBLE,
          allowNull: false,
        },
        percentage: {
          type: DataTypes.DOUBLE,
          allowNull: false,
        },
        capital: {
          type: DataTypes.DOUBLE,
          allowNull: false,
        },
        disabled: {
          type: DataTypes.BOOLEAN,
          defaultValue: false,
        },
      },
      {
        sequelize,
      }
    );
    return this;
  }

  static associate(models) {
    this.belongsTo(models.Registry);
  }
}

module.exports = Realization;
