const { DataTypes, Model } = require("sequelize");

class Registry extends Model {
  static init(sequelize) {
    super.init(
      {
        coa: {
          type: DataTypes.DOUBLE,
          allowNull: false,
        },
        meanPrice: {
          type: DataTypes.DOUBLE,
          allowNull: false,
        },
        operation: {
          type: DataTypes.ENUM,
          values: ["BUY", "SELL", "EXCHANGE", "INCOMES", "IPO", "INJECT", "WITHDRAW", "TRANSFER"],
        },
        price: {
          type: DataTypes.DOUBLE,
          allowNull: false,
        },
        qty: {
          type: DataTypes.DOUBLE,
          allowNull: false,
        },
        total: {
          type: DataTypes.DOUBLE,
          allowNull: false,
        },
      },
      {
        sequelize,
      }
    );
    return this;
  }

  static associate(models) {
    this.belongsTo(models.User);
    this.belongsTo(models.Account);
    this.belongsTo(models.Ticker);
    this.hasOne(models.Realization);
    this.hasOne(models.Rentability);
  }
}

module.exports = Registry;
