const { DataTypes, Model } = require("sequelize");

class Rentability extends Model {
  static init(sequelize) {
    super.init(
      {
        percentage: {
          type: DataTypes.DOUBLE,
          allowNull: false,
        },
        amount: {
          type: DataTypes.DOUBLE,
          allowNull: false,
        },
        capital: {
          type: DataTypes.DOUBLE,
          allowNull: false,
        },
      },
      {
        sequelize,
      }
    );
    return this;
  }

  static associate(models) {
    this.belongsTo(models.Registry);
  }
}

module.exports = Rentability;
