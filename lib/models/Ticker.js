const { DataTypes, Model } = require("sequelize");

class Ticker extends Model {
  static init(sequelize) {
    super.init(
      {
        name: {
          type: DataTypes.STRING,
          allowNull: false,
          unique: true,
        },
        description: {
          type: DataTypes.STRING,
          defaultValue: "No description",
        },
        stockPrice: {
          type: DataTypes.DOUBLE,
          allowNull: false,
        },
        currency: {
          type: DataTypes.ENUM,
          values: ["BRL", "USD"],
        },
      },
      {
        sequelize,
      }
    );
    return this;
  }

  static associate(models) {
    this.hasMany(models.Registry);
    this.hasMany(models.Asset);
  }
}

module.exports = Ticker;
