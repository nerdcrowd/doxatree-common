const { DataTypes, Model } = require("sequelize");

const generateShortCode = () => {
  const possible = "0123456789";
  let string = "";
  for (let i = 0; i < 6; i += 1) {
    string += possible.charAt(Math.floor(Math.random() * possible.length));
  }

  return string;
};

class User extends Model {
  static init(sequelize) {
    super.init(
      {
        active: {
          type: DataTypes.BOOLEAN,
          defaultValue: false,
        },
        provider: {
          type: DataTypes.ENUM,
          defaultValue: "platform",
          values: ["google", "apple", "facebook", "platform"],
        },
        name: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        avatar: {
          type: DataTypes.STRING,
          defaultValue:
            "https://upload.wikimedia.org/wikipedia/commons/7/7c/Profile_avatar_placeholder_large.png",
        },
        email: {
          type: DataTypes.STRING,
          allowNull: false,
          unique: true,
        },
        password: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        role: {
          type: DataTypes.ENUM,
          defaultValue: "basic",
          values: ["basic", "premium", "admin"],
        },
        phone: {
          type: DataTypes.BIGINT,
          allowNull: false,
        },
        ssn: {
          type: DataTypes.BIGINT,
          allowNull: false,
        },
        token: {
          type: DataTypes.STRING,
          defaultValue: null,
        },
        messageToken: {
          type: DataTypes.STRING,
          defaultValue: null,
        },
      },
      {
        sequelize,
      }
    );

    this.addHook("beforeCreate", async (user) => {
      user.messageToken = generateShortCode();
    });

    return this;
  }

  static associate(models) {
    this.hasMany(models.Portfolio);
    this.hasMany(models.Registry);
  }
}

module.exports = User;
