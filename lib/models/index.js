const User = require("./User");
const Asset = require("./Asset");
const Locket = require("./Locket");
const Group = require("./Locket");
const Account = require("./Account");
const Portfolio = require("./Portfolio");
const Registry = require("./Registry");
const Realization = require("./Realization");
const Rentability = require("./Rentability");
const Balance = require("./Balance");
const Ticker = require("./Ticker");
const Metadata = require("./Metadata");

const models = [
  Account,
  Ticker,
  User,
  Asset,
  Locket,
  Portfolio,
  Group,
  Registry,
  Rentability,
  Realization,
  Balance,
  Metadata,
];

const sync = (sequelize) => {
  models
    .map((model) => model.init(sequelize))
    .map((model) => model.associate && model.associate(sequelize.models));

  sequelize.sync();
};

module.exports = {
  sync,
};
