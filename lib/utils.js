const { BUY } = require("./constants");

module.exports = {
  calculateNewQty: (qty, operation, assets) => {
    const response = [];
    let totalMetaPercentageOfAssets = 0;
    let totalQty = operation === BUY ? qty : -qty;

    for (const asset of assets) {
      totalMetaPercentageOfAssets +=
        asset.metaPercentage * asset.Locket.metaPercentage;
      totalQty += asset.qty;
    }

    for (const asset of assets) {
      const totalPercentage =
        (asset.metaPercentage * asset.Locket.metaPercentage) /
        totalMetaPercentageOfAssets;
      asset.qty = totalQty * totalPercentage;
      response.push(Promise.resolve(asset.save()));
    }

    return response;
  },
  assertExists: (value, messageToThrow, statusCode) => {
    if (value !== undefined && value !== null) {
      return value;
    }

    throw {
      statusCode,
      message: messageToThrow,
    };
  },
  formatFloatResult: (op) => parseFloat(op.toFixed(16)),
  formatFloatPrice: (op) => parseFloat(op.toFixed(2)),
  formatFloatPercentage: (op) => parseFloat(op.toFixed(4)),
};
